# Components Builder

Example

```ts
import { Button, Components, InputText, SelectMenu } from "./mod.ts";

// All of the parameters have defaults

const it = new InputText().setCustomId("myCustomId").setStyle("Short").setPlaceHolder("my placeholder").setMinLength(1);

const but = new Button().setCustomId("myCustomId").setStyle("Danger").setLabel("Danger");

const sel = new SelectMenu()
  .addOption("item1label", "item1value")
  .addOptions([{ value: "item2value", label: "item2label" }])
  .setMinValue(1)
  .setMaxValue(2);

let comps = new Components().addComponent(it);
// or
comps = new Components().addComponent(but).addComponent(sel);
//or
comps = new Components().addComponents([but, sel]);
console.log(comps);
```
