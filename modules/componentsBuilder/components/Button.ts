import { BaseComponent } from "../base.ts";
import type { ButtonComponent } from "../deps.ts";
import { ButtonStyles, MessageComponentTypes, nanoid } from "../deps.ts";
import type { ComponentEmoji } from "../types.ts";
import { shortenString } from "../utils.ts";

export class Button extends BaseComponent implements ButtonComponent {
  label = "Default Label";
  type: MessageComponentTypes.Button = MessageComponentTypes.Button;
  style: ButtonStyles = ButtonStyles.Primary;
  emoji?: ButtonComponent["emoji"];
  url?: string;
  disabled?: boolean;

  constructor() {
    super("Button", nanoid());
  }

  /** Set different styles/colors of the buttons. */
  setStyle(style: keyof Omit<typeof ButtonStyles, "Link">) {
    this.style = ButtonStyles[style];
    // Non link buttons need a customId
    if (!this.customId) this.customId = nanoid();
    return this;
  }

  /** Set a unicode or custom Discord emoji. */
  setEmoji(emoji: ComponentEmoji) {
    this.emoji =
      typeof emoji === "string"
        ? {
            name: emoji,
          }
        : emoji;
    return this;
  }

  /** Makes this button a link button with the specified url. */
  setUrl(url: string) {
    this.style = ButtonStyles.Link;
    this.url = url;
    // Link buttons cannot have customId
    this.customId = undefined;
    return this;
  }

  setEnabled(enabled: boolean) {
    this.disabled = !enabled;
    return this;
  }

  disable() {
    this.disabled = true;
    return this;
  }

  enable() {
    this.disabled = undefined;
    return this;
  }

  setCustomId(id: string) {
    this.customId = id;
    return this;
  }

  /** Sets custom id to a random one. */
  regenCustomId() {
    this.customId = nanoid();
    return this;
  }

  /** What the button says. Max 80 characters. */
  setLabel(label: string) {
    this.label = shortenString(label, 80);
    return this;
  }
}
