import { BaseComponent } from "../base.ts";
import type { InputTextComponent } from "../deps.ts";
import { MessageComponentTypes, nanoid, TextStyles } from "../deps.ts";
import { shortenString } from "../utils.ts";

export class InputText extends BaseComponent implements InputTextComponent {
  type: MessageComponentTypes.InputText = MessageComponentTypes.InputText;
  style = TextStyles.Short;
  customId: string;
  label = "Default Label";
  placeholder?: string; // max 100 chars
  minLength?: number;
  maxLength?: number;
  required?: boolean;

  constructor() {
    super("InputText");
    this.customId = nanoid();
  }

  setStyle(style: keyof typeof TextStyles) {
    this.style = TextStyles[style];
    return this;
  }

  setCustomId(id: string) {
    this.customId = id;
    return this;
  }

  /** Sets custom id to a random one. */
  regenCustomId() {
    this.customId = nanoid();
    return this;
  }

  /** What the input says. Max 80 characters. */
  setLabel(label: string) {
    this.label = shortenString(label, 80);
    return this;
  }

  /** Input placeholder. Max 100 characters. */
  setPlaceHolder(placeholder: string) {
    this.placeholder = shortenString(placeholder, 100);
    return this;
  }

  // Min: 0, Max: 4000 https://discord.com/developers/docs/interactions/message-components#text-inputs-text-input-structure
  /** Minimum length of the text user provides */
  setMinLength(value: number) {
    if (value > 4000) value = 4000;
    if (value < 0) value = 0;
    this.minLength = value;
    return this;
  }

  // Min: 1, Max: 4000 https://discord.com/developers/docs/interactions/message-components#text-inputs-text-input-structure
  /** Maximum length of the text user provides */
  setMaxLength(value: number) {
    if (value > 4000) value = 4000;
    if (value < 1) value = 1;
    this.maxLength = value;
    return this;
  }

  /** If this input is required or not */
  setRequired(value: boolean) {
    this.required = value;
    return this;
  }
}
