import { BaseComponent } from "../base.ts";
import type { SelectMenuComponent, SelectOption } from "../deps.ts";
import { MessageComponentTypes, nanoid } from "../deps.ts";
import type { ComponentEmoji, SelectMenuOptionAdd } from "../types.ts";
import { shortenString } from "../utils.ts";

export class SelectMenu extends BaseComponent implements SelectMenuComponent {
  type: MessageComponentTypes.SelectMenu = MessageComponentTypes.SelectMenu;
  customId: string;
  placeholder?: string;
  minValues?: number;
  maxValues?: number;
  options: SelectOption[] = [];

  constructor() {
    super("SelectMenu");
    this.customId = nanoid();
  }

  /** A custom placeholder text if nothing is selected. Maximum 100 characters. */
  setPlaceholder(value: string) {
    this.placeholder = shortenString(value, 100);
    return this;
  }

  setCustomId(id: string) {
    this.customId = id;
    return this;
  }

  /** Sets custom id to a random one. */
  regenCustomId() {
    this.customId = nanoid();
    return this;
  }

  /** The minimum number of items that must be selected. Default 1. Between 1-25. */
  setMinValue(value: number) {
    if (value > 25) value = 25;
    if (value < 1) value = 1;
    this.minValues = value;
    return this;
  }

  /** The maximum number of items that can be selected. Default 1. Between 1-25. */
  setMaxValue(value: number) {
    if (value > 25) value = 25;
    if (value < 1) value = 1;
    this.maxValues = value;
    return this;
  }
  /** Adds a option
   *
   * **Max Lenghts**
   *
   * label - 25
   *
   * value - 100
   *
   * description - 50
   */
  addOption(label: string, value: string, selectedByDefault = false, description?: string, emoji?: ComponentEmoji) {
    if (this.options.length == 25) return this;
    label = shortenString(label, 25);
    value = shortenString(value, 100);
    if (description) shortenString(description, 50);
    this.options.push({
      label: label,
      value: value,
      description: description,
      emoji:
        typeof emoji === "string"
          ? {
              name: emoji,
            }
          : emoji,
      default: selectedByDefault,
    });
    return this;
  }

  /** Adds options in bulk
   *
   * **Max Lenghts**
   *
   * label - 25
   *
   * value - 100
   *
   * description - 50
   */
  addOptions(options: SelectMenuOptionAdd[]) {
    options.forEach((option) => {
      option.label = shortenString(option.label, 25);
      option.value = shortenString(option.value, 100);
      if (option.description) option.description = shortenString(option.description, 50);
      if (options.length == 25) return this;
      this.options.push({
        ...option,
        default: option.default || false,
      });
    });
    return this;
  }
}
