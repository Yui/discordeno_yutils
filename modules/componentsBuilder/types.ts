import type { Button } from "./components/Button.ts";
import type { InputText } from "./components/InputText.ts";
import type { SelectMenu } from "./components/SelectMenu.ts";
import { ButtonComponent, SelectOption } from "./deps.ts";

export type ComponentAddables = Button | SelectMenu | InputText;
export type ComponentAddableSelectMenuOption = "";

export interface SelectMenuOptionAdd extends Omit<SelectOption, "default"> {
  default?: boolean;
}

export type ComponentEmoji = string | ButtonComponent["emoji"];
