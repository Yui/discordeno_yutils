// Length starts from the index
export function shortenString(text: string, length: number, startIndex = 0): string {
  if (startIndex + text.length < length) return text;
  return text.substring(startIndex, startIndex + length);
}
